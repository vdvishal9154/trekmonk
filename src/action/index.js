import api from '../api';
import types from './types';
import history from '../history';

export const login = formValues => async (dispatch) => {
  const response = await api.post('/login', formValues);

  dispatch({
    type: types.LOGIN,
    payload: response.data,
  });
};

export const signUp = formValues => async (dispatch) => {
  const response = await api.post('/signUp', formValues);

  dispatch({
    type: types.SIGN_UP,
    payload: response.data,
  });
  history.push('/login');
};

export const profile = (id, token) => async (dispatch) => {
  const response = await api.get(`/api/users/${id}`, {
    Authorisation: token,
  });

  dispatch({
    type: types.GET_PROFILE,
    payload: response.data,
  });
};

export const editProfile = (id, formValues) => async (dispatch) => {
  const response = await api.put(`/profile/${id}`, formValues);

  dispatch({
    type: types.EDIT_PROFILE,
    payload: response.data,
  });
};


export const productFilter = filters => async (dispatch) => {
  console.log('________________', filters);
  const response = await api.get(`/products?gender=${filters.gender || ''}`);

  // const response = await api.get(`/products/?id=${filters.id || ''}&title=${filters.title || ''}&description=${filters.description || ''}&price=${filters.price || ''}&gender=${filters.gender || ''}&variation__color=${filters.variation__color || ''}&size=${filters.size || ''}&category__title=${filters.category__title || ''}&active=${filters.active || ''}&category=${filters.category || ''}`);
  dispatch({
    type: types.PRODUCT_FILTER,
    payload: response.data,
  });
};

export const productSingleFilter = filters => async (dispatch) => {
  console.log('________________', filters);

  const response = await api.get(`/products?id=${filters.id || ''}`);

  // const response = await api.get(`/products/?id=${filters.id || ''}&title=${filters.title || ''}&description=${filters.description || ''}&price=${filters.price || ''}&gender=${filters.gender || ''}&color=${filters.color || ''}&size=${filters.size || ''}&category__title=${filters.category__title || ''}&active=${filters.active || ''}&category=${filters.category || ''}`);

  dispatch({
    type: types.PRODUCT_FILTER_SINGLE,
    payload: response.data,
  });
};

export const productFeatured = () => async (dispatch) => {
  // const response = await api.get('/products/featured');
  const response = await api.get('/products?featured=true');
  console.log('________________', response);

  dispatch({
    type: types.PRODUCT_FEATURED,
    payload: response.data,
  });
};

export const productCategory = () => async (dispatch) => {
  const response = await api.get('/products/category');

  dispatch({
    type: types.PRODUCT_CATEGORY,
    payload: response.data,
  });
};
