export default (state = [], action) => {
  switch (action.type) {
    case 'PRODUCT_FILTER':
      return action.payload;
    default:
      break;
  }
  return state;
};
