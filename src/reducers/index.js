import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import products from './products';
import product from './product';
import category from './category';
import featured from './featured';
import profile from './profile';


export default combineReducers({
  products,
  profile,
  featured,
  category,
  product,
  form: formReducer,
});
