import React from 'react';
import {
  Row, Col, Button, Divider,
} from 'antd';
import './blogs.less';

class Blog extends React.Component {
  render() {
    return (
      <div className="">
        <div className="card">
          <Row type="flex" justify="center">
            <Col xs={24} sm={24} md={24} lg={24} xl={12}>
              <div style={{ textAlign: 'center' }}>
                <h1 style={{ textAlign: 'center', fontWeight: '700' }}> FRESH FROM OUR BLOG</h1>
              </div>
              <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
                <div style={{ width: '32%', margin: '10px' }}>
                  <img src="https://images.unsplash.com/photo-1571649454759-c0de91fe6044?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80" style={{ width: '100%', height: '250px', objectFit: 'cover' }} />
                  <div>
                    <h2>Blog Title</h2>
                    <div style={{ marginTop: '5px' }}>
                    By Author
                    </div>
                    <div>
                    20th March 2018
                    </div>
                    <Divider />
                    <div style={{ margin: '2.5px 2.55px' }}>
                Typography is the work of typesetters,

                    </div>
                    <div className="uniMargin">
                      <Button style={{ marginRight: '5px' }}>
                    READ
                      </Button>
                      <Button>
                    VIEW MORE
                      </Button>
                    </div>
                  </div>

                </div>

                <div style={{ width: '32%', margin: '10px' }}>
                  <img src="https://images.unsplash.com/photo-1571642376444-52558bb8cee2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1326&q=80" style={{ width: '100%', height: '250px', objectFit: 'cover' }} />
                  <div>
                    <h2>Blog Title</h2>
                    <div style={{ marginTop: '5px' }}>
                    By Author
                    </div>
                    <div>
                    20th March 2018
                    </div>
                    <Divider />
                    <div style={{ margin: '2.5px 2.55px' }}>
                Typography to lem help of typesetters,

                    </div>
                    <div className="uniMargin">
                      <Button style={{ marginRight: '5px' }}>
                    READ
                      </Button>
                      <Button>
                    VIEW MORE
                      </Button>
                    </div>
                  </div>
                </div>

                <div style={{ width: '32%', margin: '10px' }}>
                  <img src="https://images.unsplash.com/photo-1571672840242-a1030a497310?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80" style={{ width: '100%', height: '250px', objectFit: 'cover' }} />
                  <div>
                    <h2>Blog Title</h2>
                    <div style={{ marginTop: '5px' }}>
                    By Author
                    </div>
                    <div>
                    20th March 2018
                    </div>
                    <Divider />
                    <div style={{ margin: '2.5px 2.55px' }}>
                Typography is the work of typesetters,

                    </div>
                    <div className="uniMargin">
                      <Button style={{ marginRight: '5px' }}>
                    READ
                      </Button>
                      <Button>
                    VIEW MORE
                      </Button>
                    </div>
                  </div>
                </div>

              </div>
            </Col>
            <Col xs={24} sm={24} md={24} lg={24} xl={6} style={{ borderLeft: '1px solid black' }}>
              <div style={{ textAlign: 'center' }}>
                <h1 style={{ textAlign: 'center', fontWeight: '700' }}>OTHER BlOG ARTICLES</h1>
              </div>
              <div style={{ display: 'flex', flexDirection: 'column' }}>
                <div style={{ display: 'flex', flexDirection: 'row', margin: '10px' }}>
                  <div>
                    <img src="https://images.unsplash.com/photo-1571649454759-c0de91fe6044?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80" style={{ width: '100px', height: '100px', objectFit: 'cover' }} />
                  </div>
                  <div style={{ width: '32%', margin: '0 10px' }}>
                    <h2>Blog Title</h2>
                    <div style={{ marginTop: '5px' }}>
                    By Author
                    </div>
                    <div>
                    20th March 2018
                    </div>
                  </div>
                  <div style={{ display: 'flex', alignItems: 'flex-end' }}>
                    <Button>
                      Read
                    </Button>
                  </div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'row', margin: '10px' }}>
                  <div>
                    <img src="https://images.unsplash.com/photo-1571672840242-a1030a497310?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80" style={{ width: '100px', height: '100px', objectFit: 'cover' }} />
                  </div>
                  <div style={{ width: '32%', margin: '0 10px' }}>
                    <h2>Blog Title</h2>
                    <div style={{ marginTop: '5px' }}>
                    By Author
                    </div>
                    <div>
                    20th March 2018
                    </div>
                  </div>
                  <div style={{ display: 'flex', alignItems: 'flex-end' }}>
                    <Button>
                      Read
                    </Button>
                  </div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'row', margin: '10px' }}>
                  <div>
                    <img src="https://images.unsplash.com/photo-1571672840242-a1030a497310?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80" style={{ width: '100px', height: '100px', objectFit: 'cover' }} />
                  </div>
                  <div style={{ width: '32%', margin: '0 10px' }}>
                    <h2>Blog Title</h2>
                    <div style={{ marginTop: '5px' }}>
                    By Author
                    </div>
                    <div>
                    20th March 2018
                    </div>
                  </div>
                  <div style={{ display: 'flex', alignItems: 'flex-end' }}>
                    <Button>
                      Read
                    </Button>
                  </div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'row', margin: '10px' }}>
                  <div>
                    <img src="https://images.unsplash.com/photo-1571672840242-a1030a497310?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80" style={{ width: '100px', height: '100px', objectFit: 'cover' }} />
                  </div>
                  <div style={{ width: '32%', margin: '0 10px' }}>
                    <h2>Blog Title</h2>
                    <div style={{ marginTop: '5px' }}>
                    By Author
                    </div>
                    <div>
                    20th March 2018
                    </div>
                  </div>
                  <div style={{ display: 'flex', alignItems: 'flex-end' }}>
                    <Button>
                      Read
                    </Button>
                  </div>
                </div>

              </div>
            </Col>
            {/* <Col xs={24} sm={24} md={24} lg={12} xl={8} style={{ margin: '0 10px', backgroundColor: 'white!important' }}>
              <div className="colBlog">
                <img src="https://images.unsplash.com/photo-1542359649-31e03cd4d909?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=967&q=80" alt="Blog" />
              </div>
            </Col>
            <Col xs={24} sm={24} md={24} lg={8} xl={8} style={{ margin: '0 10px', backgroundColor: 'white!important' }}>
              <div className="colStyle">
                <h1>THE LATEST FROM BLOG </h1>
                <hr />
                <div style={{ marginTop: '15px' }}>
                  <div className="headStyle">
                   Blog Title
                  </div>
                  <div style={{ marginTop: '5px' }}>
                    By Author
                  </div>
                  <div>
                    20th March 2018
                  </div>
                  <div style={{ margin: '15px 5px' }}>
                Typography is the work of typesetters,

                  </div>
                  <div className="uniMargin">
                    <Button style={{ marginRight: '5px' }}>
                    READ
                    </Button>
                    <Button>
                    VIEW MORE
                    </Button>
                  </div>

                </div>
              </div>
              <Row />
            </Col>
           */}
          </Row>
        </div>
      </div>
    );
  }
}

export default Blog;
