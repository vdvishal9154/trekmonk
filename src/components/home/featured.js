import React, { useState } from 'react';
import {
  Row, Col, Icon,
} from 'antd';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import Modal from 'react-modal';
import StarRatings from 'react-star-ratings';

import QuickView from '../reusable/quickview';
import ColorBox from '../reusable/colorBox';
import Spinner from '../reusable/spinner';

import './featured.less';

const settings = {
  className: 'center',
  centerPadding: '60px',
  slidesToShow: 4,
  slidesToScroll: 1,
  swipeToSlide: true,
  dots: true,
  responsive: [
    {
      breakpoint: 1440,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,

        infinite: true,
      },
    }, {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,


      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,

      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: '50%',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: 'auto',
  },
};

function Featured(props) {
  const [quickView, setQuickView] = useState(false);
  const [product, setProduct] = useState(null);

  const showQuickView = (prod) => {
    setQuickView(true);
    setProduct(prod);
  };

  const closeModal = () => {
    setQuickView(false);
  };

  const reviewFeatured = () => {
    if (props.featured && props.featured.length !== 0) {
      return (props.featured.map(featured => (
        <div className="animated 1 zoomIn delay-0.25s" key={featured.id}>
          <Link to={{ pathname: '/product', search: `?id=${featured.id}` }}>
            <div className="imgMainDiv">

              <div className="imgDiv">


                <img src={featured.img_1} height="50%" width="75%" alt="Featured Img" />
              </div>

              <div>Price: {featured.price}
                <br />
            Name: {featured.title}
              </div>
              <StarRatings
                rating={3}
                starRatedColor="red"
          // changeRating={this.changeRating}
                numberOfStars={5}
                name="rating"
                starDimension="12px"
                starSpacing="2px"
              />
              {/* <ColorBox colors={featured.variation_set} /> */}

            </div>
          </Link>
          <div style={{ display: 'flex', justifyContent: 'center', margin: '10px' }}>
            <div
              className="iconStyle"
              style={{
              }}
            >
              <Icon type="shopping-cart" />
            </div>
            <div
              className="iconStyle"
              onClick={() => showQuickView(featured)}
            >
              <Icon type="eye" theme="filled" />
            </div>

          </div>
        </div>
      )));
    }
    return (
      <div>
        <Spinner height="250px" width="90vw" />
      </div>
    );
  };

  const renderQuickView = () => (
    <Modal
      contentLabel="Basic Modal"
      isOpen={quickView}
      style={customStyles}
      onRequestClose={() => closeModal()}
    >
      <div>
        <Row type="flex" justify="end">
          <Icon type="close" onClick={() => closeModal()} />
        </Row>
        <QuickView product={product} />
      </div>

    </Modal>

  );

  return (
    <div className="featured">
      <Row type="flex" justify="center" align="middle">
        <Col className="featTitle" span={24}>
          <h3>
          FEATURED
          </h3>
          <span>
          Shop our featured products
          </span>
        </Col>
      </Row>
      {/* <Row type="flex" justify="center" align="middle"> */}
      <div className="topDiv">
        <Slider {...settings}>

          {reviewFeatured()}

        </Slider>
      </div>
      {quickView ? renderQuickView() : null}
    </div>
  );
}


export default Featured;
