import React from 'react';


import { connect } from 'react-redux';
import HeaderS from './header';
import Main from './main';
import Featured from './featured';
import Category from './category';
import Blog from './blogs';
import Footer from './footer';
import Insta from './insta';

import { productFeatured, productSingleFilter } from '../../action';

class Home extends React.Component {
  state = {

  }

  componentDidMount() {
    this.props.dispatchFeatured();
    this.props.dispatchSingleFilter();
  }

  render() {
    return (
      <div>
        <HeaderS />
        <Main />
        <Category />
        <Featured featured={this.props.featured} />
        <Blog />
        <Insta />
        <Footer />

      </div>
    );
  }
}


const mapDispatchToProps = dispatch => ({
  dispatchFeatured: () => dispatch(productFeatured()),
  dispatchSingleFilter: () => dispatch(productSingleFilter({ miscId: 1 })),
});

const mapStateToProps = state => ({
  featured: state.featured,
  product: state.products,
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
