/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import { Link } from 'react-router-dom';
import VisibilitySensor from 'react-visibility-sensor';

import { Row, Col, Button } from 'antd';

import './category.less';

class Category extends React.Component {
  state={
    visible: null,
    visible2: null,
  }

  onChange = (isVisible) => {
    isVisible ? this.setState(
      { visible: true },
    ) : this.setState(
      { visible: false },
    );
  }

  onChange2 = (isVisible) => {
    isVisible ? this.setState(
      { visible2: true },
    ) : this.setState(
      { visible2: false },
    );
  }

  render() {
    return (
      <div className="main">
        <Row className="headStyle" type="flex" justify="center" align="middle">
          <Col xs={24} sm={24} md={24} lg={10} xl={9} style={{ margin: '0 10px ' }}>
            <Row>
              <div className="boxMain">

                <VisibilitySensor
                  onChange={this.onChange}
                  offset={{ top: -100, bottom: -100 }}
                >

                  <Link to={{ pathname: '/collections', search: '?gender=women' }}>

                    <div className="boxWomen animated 1 fadeIn delay-0.25s " />
                    {(this.state.visible) ? (
                      <div className="boxTitle animated 1 fadeInRight delay-0.25s">
                        <h3>
  WOMEN COLLECTIONS
                        </h3>
                        <div>
                          <h4>
SHOP
                          </h4>
                        </div>


                      </div>

                    ) : (
                      <div className="boxTitle animated 1 fadeOutRight delay-0.25s">
                        <h3>
  WOMEN COLLECTIONS
                        </h3>
                        <div>
                          <h4>
SHOP
                          </h4>
                        </div>


                      </div>

                    )}
                  </Link>
                </VisibilitySensor>
              </div>

            </Row>
            <Row>


              <div className="boxBags">
                <div className="text">
                  <strong>BAGS</strong>
                </div>
                <div className="subCategoryWomen">
                  <Button type="link" ghost>Shirts
                  </Button>
                  <br />
                  <Button type="link" ghost>Pants
                  </Button>
                  <br />
                  <Button type="link" ghost>Shirts
                  </Button>
                  <br />
                  <Button type="link" ghost>Fleece
                  </Button>
                  <br />
                </div>
              </div>


            </Row>

          </Col>
          <Col xs={24} sm={24} md={24} lg={10} xl={9} style={{ margin: '0 10px ' }}>
            <Row>
              <div className="boxAccessories">
                <div className="text"><strong>ACCESSORIES</strong>
                </div>
                <div className="subCategoryWomen">
                  <Button type="link" ghost>Shirts
                  </Button>
                  <br />
                  <Button type="link" ghost>Pants
                  </Button>
                  <br />
                  <Button type="link" ghost>Shirts
                  </Button>
                  <br />
                  <Button type="link" ghost>Fleece
                  </Button>
                  <br />
                </div>
              </div>
            </Row>
            <Row>
              <VisibilitySensor
                onChange={this.onChange2}
                offset={{ top: -100, bottom: -100 }}
              >
                <div className="boxMain">

                  <Link to={{ pathname: '/collections', search: '?gender=men' }}>
                    <div className="boxMen" />
                    {(this.state.visible2) ? (
                      <div className="boxTitle animated 1 fadeInRight delay-0.25s">
                        <h3>
                       WOMEN COLLECTIONS
                        </h3>
                        <div>
                          <h4>
                  SHOP
                          </h4>
                        </div>


                      </div>

                    ) : (
                      <div className="boxTitle  animated 1 fadeOutRight delay-0.25s">
                        <h3>
                     WOMEN COLLECTIONS
                        </h3>
                        <div>
                          <h4>
                SHOP
                          </h4>
                        </div>


                      </div>


                    )}
                  </Link>
                </div>
              </VisibilitySensor>
            </Row>
          </Col>
        </Row>

      </div>
    );
  }
}


export default Category;
