/* eslint-disable jsx-a11y/media-has-caption */
import React from 'react';
import Slider from 'react-slick';
import './main.less';


const settings = {
  className: 'center',
  infinite: true,
  centerPadding: '60px',
  slidesToShow: 1,
  swipeToSlide: true,
};
class Main extends React.Component {
  render() {
    return (
      <div className="sliderTop">

        <Slider
          {...settings}

        >
          <div
            className="extra"
          >            <img src="https://cdn.shopify.com/s/files/1/0036/7306/3491/files/dome1-bnr2.jpg?v=1564398860" />

            <div className="mainText animated 1 fadeIn delay-0.5s">
              <h1>
                The Best Winter Collection
              </h1>
              <div>
                Shop Now
              </div>
            </div>
          </div>
          <div
            className="extra"
          >
            <img src="https://cdn.shopify.com/s/files/1/0036/7306/3491/files/hero_1200x.jpg?v=1561803931" />
          </div>
          <div
            className="extra"
          >
            <img src="https://cdn.shopify.com/s/files/1/0036/7306/3491/files/best-shopify-theme_1200x.jpg?v=1564548324" />

          </div>
        </Slider>
      </div>
    );
  }
}


export default Main;
