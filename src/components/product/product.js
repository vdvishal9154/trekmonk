import React from 'react';
import {
  Row, Col, Tabs, Button, InputNumber, Popover, Drawer, notification,
} from 'antd';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as _ from 'lodash';

import { Carousel } from 'react-responsive-carousel';
import queryString from 'query-string';
import StarRatings from 'react-star-ratings';

import { productSingleFilter, productFeatured } from '../../action';


import 'react-responsive-carousel/lib/styles/carousel.min.css';

import Header from '../header/header';
import ColorBox from '../reusable/colorBox';
import Spinner from '../reusable/spinner';

import './product.less';

const { TabPane } = Tabs;
let sizes = [];
const colors = [];
class Product extends React.Component {
  state = {
    loader: true,
    selectedPro: null,
    activeSize: null,
    activeColor: null,
    counter: 10,
    color: null,
    visible: false,
    cart: { quantity: 1 },

  }

  componentDidMount() {
    const values = queryString.parse(this.props.location.search);

    this.props.dispatchFeatured();
    this.props.dispatchFilter(values);

    this.setState({
      selectedPro: values.id,
    });
    this.stateTrue(values.id);
  }

  componentDidUpdate(prevProps) {
    const oldId = prevProps.location.search;
    const newId = this.props.location.search;

    const prev = (prevProps.product);
    const latProp = (this.props.product);

    if (newId !== oldId) {
      const values = queryString.parse(this.props.location.search);
      this.props.dispatchFeatured();
      this.props.dispatchFilter(values);
    }

    if (!_.isEqual(prev, latProp)) {
      this.setState({
        loader: false,
      });
    }

    if (this.props.product[0]) {
      this.props.product[0].variation_set.map((size) => {
        sizes = new Set([...sizes, size.size]);
      });
      sizes = Array.from(sizes);
    }
  }

  stateTrue = (pro) => {
    if (this.state.selectedPro === null) {
      this.setState({
        loader: true,
        selectedPro: pro,
      });
    } else if (this.state.selectedPro !== pro) {
      this.setState({
        loader: true,
        selectedPro: pro,
      });
    } else {
      this.setState({
        loader: false,
        selectedPro: pro,
      });
    }
  }

  showDrawer = () => {
    if (this.state.cart !== null && this.state.cart.color !== null && this.state.cart.color !== undefined) {
      this.setState({
        visible: true,
      });
    } else {
      notification.info({
        message: 'Please select a color',
      });
    }
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

reviewrender = () => {
  if (this.props.product[0].review_set.length !== 0) {
    return (this.props.product[0].review_set.map((review, index) => (
      <div key={index}>
        <div style={{ fontSize: '20px' }}>
          {review.comment}
        </div>
        <StarRatings
          rating={review.rating}
          starRatedColor="black"
          // changeRating={this.changeRating}
          numberOfStars={5}
          name="rating"
          starDimension="40px"
          starSpacing="5px"
        />
        <hr />
      </div>
    )));
  }
  return (
    <div>
        No reviews yet
    </div>
  );
}


colorArr = () => {
  if (this.state.color !== null) {
    return (this.state.color.map(color => (
      <Popover content={color.color} style={{ backgroundColor: '#black' }} key={color.color}>
        <div
          className="colorBox"
          style={{
            backgroundColor: color.color,
            height: '22px',
            width: '22px',

            display: 'inline-block',
            margin: '1px',
            border: this.setColor(color.color),
            borderRadius: '50%',
          }}
          onClick={() => this.selectedColor(color)}
        />
      </Popover>
    )));
  }
};

selectedColor = (color) => {
  if (this.state.activeColor === color.color) {
    this.setState(prevState => ({
      activeColor: null,
      counter: null,
      cart: { ...prevState.cart, color: null },
    }));
  } else {
    this.setState(prevState => ({
      activeColor: color.color,
      counter: color.inventory,
      cart: {
        ...prevState.cart, color: color.color, title: color.title, quantity: 1, quantityMax: color.inventory, price: this.props.product[0].price,
      },
    }));
  }
}

setColor = (color) => {
  if (this.state.activeColor === color) {
    return '2px double black';
  }
  return '2px solid white';
}

sizeRender = () => this.props.product[0].variation_set.map(size => (
  <span
    className="hoverBox"
    style={{ color: this.selectedSizeBack(size.size), backgroundColor: this.selectedSize(size.size) }}
    key={size.size}
    onClick={() => this.selectSize(size)}
  >{size.size}
  </span>
))

renderFeatured = () => this.props.featured.slice(0, 3).map(product => (
  <Link
    to={{ pathname: '/product', search: `?id=${product.id}` }}
    onClick={() => this.stateTrue(`${product.id}`)
}
    key={product.id}
  >
    <div style={{ display: 'flex', width: '100%', margin: '10px' }}>
      <img src={product.img_1} height="200px" width="150px" style={{ margin: '0 10px' }} alt="Product" />
      <div>
        <h3>Name: {product.title}</h3>
        <h6>Price: {product.price}</h6>
      </div>

    </div>
  </Link>
))

selectSize = (size) => {
  if (this.state.activeSize === size.size) {
    this.setState({ activeSize: null, color: null, cart: null });
    this.colorArr();
  } else {
    this.setState(prevState => ({
      activeSize: size.size,
      color: size.inventory,
      basePrice: this.props.product[0].price,
      cart: {
        ...prevState.cart, size: size.size, image: this.props.product[0].img_1, price: this.props.product[0].price, quantity: 1, quantityMax: prevState.counter,
      },
    }));
    this.colorArr();
  }
}

selectedSize = (size) => {
  if (this.state.activeSize === size) {
    return 'black';
  }
  return '';
}

 selectedSizeBack = (size) => {
   if (this.state.activeSize === size) {
     return 'white';
   }
   return '';
 }

 setQuantity = (value) => {
   this.setState(prevState => ({
     cart: {
       ...prevState.cart, quantity: value, price: prevState.basePrice * value, quantityMax: prevState.counter,
     },
   }));
 }

 render() {
   const [product] = this.props.product;
   const productImg = [];
   if (product) {
     if (product.img_1 !== null) {
       productImg.push(product.img_1);
     }
     if (product.img_2 !== null) {
       productImg.push(product.img_2);
     }
     if (product.img_3 !== null) {
       productImg.push(product.img_3);
     }
     if (product.img_4 !== null) {
       productImg.push(product.img_4);
     }
     if (product.img_5 !== null) {
       productImg.push(product.img_5);
     }
     if (product.img_6 !== null) {
       productImg.push(product.img_6);
     }
   }
   if (!this.state.loader) {
     return (
       <div>
         <Header />

         <div className="main ">
           <Row type="flex" justify="center">
             <Col className="animateLayout" xs={24} sm={24} md={24} lg={8} xl={8} type="flex" justify="center">
               <Carousel
                 width="90%"
                 showStatus={false}
                 infiniteLoop
               >
                 {productImg.map((imgs, index) => (
                   <div key={index}>
                     <img src={imgs} style={{ maxHeight: '750px', width: '100%' }} alt="product" />
                   </div>

                 ))}
               </Carousel>
             </Col>


             <Col className="blogHead" xs={24} sm={24} md={24} lg={6} xl={6}>
               <div>
                 <h3>{product.title}</h3>
                 <h2>{product.price}</h2>
                 <p>
                   {product.description}
                 </p>
                 <div>
                   {this.colorArr()}
                 </div>
                 <div />
                 <div style={{ marginBottom: '10px' }}>
                   <h3>
                Size
                   </h3>
                   <div>
                     {this.sizeRender()}
                   </div>
                 </div>
                 <div style={{ marginBottom: '10px' }}>
                   <InputNumber defaultValue={1} min={0} max={this.state.counter > 10 ? 10 : this.state.counter} step={1} onChange={this.setQuantity} />
                 </div>
                 <div>
                   <Button onClick={this.showDrawer}>
                  ADD TO CART
                   </Button>
                 </div>
                 <hr />
                 <div style={{ lineHeight: '25px', marginTop: '25px' }}>
              Vendor: Gecko
                   <br />
  Categories: Bags, Clothing, Sunglasses
                   <br />
  Tags:   Clothing, Color Black, Color Blue, Color Green, Color Red, Price $7-$50, short
                 </div>
               </div>
             </Col>

             <Col className="blogHead" xs={24} sm={24} md={24} lg={4} xl={4}>
               <div>
                 <h1>
                    You may also like
                 </h1>
               </div>
               <hr />

               {this.renderFeatured()}
             </Col>

           </Row>
           <Row type="flex" justify="center">
             <Col xs={22} sm={22} md={22} lg={20} xl={20} style={{ display: 'flex', justifyContent: 'center' }}>
               <Tabs defaultActiveKey="1" style={{ width: '100%' }}>
                 <TabPane tab="DESCRIPTION" key="1">
                   {product.description}
                 </TabPane>
                 <TabPane tab="SHIPPING" key="2">
                Estimated between Fri Aug 23 and Wed Aug 28

  Will usually ship within 1 bussiness day.
                 </TabPane>
                 <TabPane tab="WHY BUY FROM US" key="3">
        Content of Tab Pane 3
                 </TabPane>
                 <TabPane tab="REVIEWS" key="4">
                   {this.reviewrender()}
                 </TabPane>
               </Tabs>

             </Col>
           </Row>
           <Row />
         </div>

         <Drawer
           title="Cart"
           placement="right"
           closable={false}
           width="320px"
           onClose={this.onClose}
           visible={this.state.visible && this.state.cart !== null ? this.state.visible : false}

         >
           {this.state.cart !== null ? (
             <div style={{ display: 'flex', flexDirection: 'row', padding: 0 }}>
               <div>
                 <img src={this.state.cart.image} width="64px" height="64px" />
               </div>
               <div style={{
                 display: 'flex', flexDirection: 'column', marginLeft: '5px', width: '100%',
               }}
               >
                 <div style={{
                   display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignContent: 'space-between',
                 }}
                 >
                   <h3>{this.state.cart.title}</h3>
                   <h3>INR {this.state.cart.price}</h3>
                 </div>

                 <div style={{
                   display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
                 }}
                 >
                   <h3 style={{
                     margin: 0,
                   }}
                   >Size: {this.state.cart.size}
                   </h3>
                   <div
                     className="colorBox"
                     style={{
                       backgroundColor: this.state.cart.color,
                       height: '22px',
                       width: '22px',

                       display: 'inline-block',
                       margin: '1px',
                       border: this.setColor(this.state.cart.color),
                       borderRadius: '50%',
                     }}
                   />

                   <InputNumber defaultValue={this.state.cart.quantity} min={1} max={this.state.cart.quantityMax > 10 ? 10 : this.state.cart.quantityMax} step={1} onChange={this.setQuantity} value={this.state.cart.quantity} />

                 </div>

               </div>

             </div>
           ) : <div /> }

           <div
             style={{
               position: 'absolute',
               right: 0,
               bottom: 0,
               width: '100%',
               borderTop: '1px solid #e9e9e9',
               padding: '10px 16px',
               background: '#fff',
               textAlign: 'right',
             }}
           >
             <Button onClick={this.onClose} style={{ marginRight: 8 }}>
              Cancel
             </Button>
             <Button onClick={this.onClose} type="primary">
              Checkout
             </Button>
           </div>
         </Drawer>
       </div>
     );
   }

   return (
     <Spinner />
   );
 }
}


const mapDispatchToProps = dispatch => ({
  dispatchFeatured: () => dispatch(productFeatured()),
  dispatchFilter: values => dispatch(productSingleFilter(values)),
});
const mapStateToProps = state => ({ product: state.product, featured: state.featured });


export default connect(mapStateToProps, mapDispatchToProps)(Product);
