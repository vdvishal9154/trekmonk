import React from 'react';
import PropagateLoader from 'react-spinners/PropagateLoader';
import { css } from '@emotion/core';


function Spinner(props) {
  const style = {
    height: props.height,
    width: props.width || 'auto',
  };
  return (
    <div
      className="CircleLoader"
      style={style}
    >
      <PropagateLoader
        css={css`
        display: block;
        margin: 0 auto;
        border-color: #36D7B7;
        opacity: 0.80;

        `}
        sizeUnit="px"
        size={25}
        color="#36D7B7"
        loading="true"
      />
    </div>
  );
}

export default Spinner;
