import React from 'react';
import { Popover } from 'antd';

function Colorbox(props) {
  const colorArr = () => props.colors.map(color => (
    <Popover content={color.color} style={{ backgroundColor: '#black' }} key={color.id}>
      <div
        className="colorBox"
        style={{
          backgroundColor: color.color,
          height: '15px',
          width: '15px',
          borderRadius: '50%',
          display: 'inline-block',
          margin: '2px',
        }}

      />
    </Popover>
  ));
  return (
    <div>
      {colorArr()}
    </div>
  );
}

export default Colorbox;
