import React from 'react';
import {
  Row, Col, Button, Divider,
} from 'antd';
import Slider from 'react-slick';
import StarRatings from 'react-star-ratings';
import ColorBox from './colorBox';

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  swipeToSlide: true,
};

class QuickView extends React.Component {
  render() {
    const { product } = this.props;
    console.log(product);
    const productImg = [];
    if (product && product.length !== 0) {
      if (product.img_1 !== null) {
        productImg.push(product.img_1);
      }
      if (product.img_2 !== null) {
        productImg.push(product.img_2);
      }
      if (product.img_3 !== null) {
        productImg.push(product.img_3);
      }
      if (product.img_4 !== null) {
        productImg.push(product.img_4);
      }
      if (product.img_5 !== null) {
        productImg.push(product.img_5);
      }
      if (product.img_6 !== null) {
        productImg.push(product.img_6);
      }
    }
    return (
      <Row type="flex" justify="space-around">
        <Col
          className="animateLayout"
          xs={24}
          sm={24}
          md={24}
          lg={8}
          xl={8}
          style={{
            display: 'flex', justifyContent: 'center', alignItems: 'center', alignContent: 'center',
          }}
        >
          <div style={{ width: '100%' }}>

            <Slider
              {...settings}
            >
              {productImg.map((imgs, index) => (

                <div key={index}>
                  <img src={imgs} style={{ maxHeight: '500px', width: '100%' }} alt="Product " />
                </div>

              ))}
            </Slider>
          </div>
        </Col>


        <Col className="blogHead" xs={24} sm={24} md={24} lg={12} xl={12}>
          <div>
            <h3>{product.title}</h3>
            <h2 style={{ margin: 0 }}>{product.price}</h2>
            <StarRatings
              rating={5}
              starRatedColor="red"
          // changeRating={this.changeRating}
              numberOfStars={5}
              name="rating"
              starDimension="12px"
              starSpacing="2px"
            />
            <p style={{ cursor: 'pointer' }}>
              {product.review_set.length} reviews
            </p>
            <Divider />
            <p>
              {product.description}
            </p>
            <div>
              <ColorBox colors={product.variation_set} />
            </div>
            <div />
            <div style={{ marginBottom: '10px' }}>
              <h3>
                Size
              </h3>
              <div>
                {product.variation_set.map(size => (
                  <span className="hoverBox" key={size.size}>{size.size}</span>
                ))}
              </div>
            </div>
            <div>
              <Button>
                  ADD TO CART
              </Button>
            </div>
            <hr />
            <div style={{ lineHeight: '25px', marginTop: '25px' }}>
              Vendor: Gecko
              <br />
  Categories: Bags, Clothing, Sunglasses
              <br />
  Tags:   Clothing, Color Black, Color Blue, Color Green, Color Red, Price $7-$50, short
            </div>
          </div>
        </Col>
      </Row>

    );
  }
}

export default QuickView;
