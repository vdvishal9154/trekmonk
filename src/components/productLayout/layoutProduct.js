/* eslint-disable no-tabs */
import React from 'react';

import {
  Row, Col, Slider, Button, Icon, Select,
} from 'antd';
import Modal from 'react-modal';

import queryString from 'query-string';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import StarRatings from 'react-star-ratings';

import { productFilter, productCategory, productSingleFilter } from '../../action';


import Header from '../header/header';
import Drawer from '../reusable/drawer';
import ColorBox from '../reusable/colorBox';
import Spinner from '../reusable/spinner';
import QuickView from '../reusable/quickview';

import './layout.less';

const { Option } = Select;
let oldId;
let newId;
let prev;
let latProp;
let values;
const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: '50%',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};
class ProductLayout extends React.Component {
	state = {
	  gender: null,
	  categoryName: null,
	  loader: false,
	  selectedCat: null,
	  quickView: false,
	  quickViewId: null,
	}


	componentDidMount() {
	  const value = queryString.parse(this.props.location.search);

	  this.props.dispatchFilter(value);
	  this.props.dispatchCategory();
	  this.props.dispatchSingleFilter();
	  this.setState({
	    gender: value.gender,
	    selectedCat: value.category,
	    categoryName: value.category || `${value.gender} All Products`,
	  });
	}

	componentDidUpdate(prevProps) {
	   oldId = prevProps.location.search;
	   newId = this.props.location.search;
	   values = queryString.parse(this.props.location.search);

	  prev = (prevProps.products);
	  latProp = (this.props.products);

	  if (!_.isEqual(prev, latProp)) {
	    this.setState({
	      loader: false,
	    });
	  }

	  if (newId !== oldId) {
	    this.props.dispatchFilter(values);
	    this.props.dispatchSingleFilter();

	    this.setState({
	      gender: values.gender,
	      categoryName: values.category || 'All Products',
	    });
	  }
	}

  showQuickView = (id) => {
    console.log(id);
    this.setState({
      quickView: true,
      quickViewId: id,
    });
  };

	onXClose = (event) => {
	  this.setState({
	    visible: event,
	  });
	};

  showDrawer = () => {
	  this.setState({
	    visible: true,
	  });
  };

  closeModal = () => {
	  this.setState({ quickView: false });
  }


  handleSorting = (value) => {
	  switch (value) {
	    case 'nameAs':
	      this.setState({
	        sort: 'name',
	        direction: 'as',
	      });
	      break;
	    case 'nameDs':
	      this.setState({
	        sort: 'name',
	        direction: 'ds',
	      });
	      break;
	    case 'priceAs':
	      this.setState({
	        sort: 'price',
	        direction: 'as',
	      });
	      break;
	    case 'priceDs':
	      this.setState({
	        sort: 'price',
	        direction: 'ds',
	      });
	      break;
	    default:
	      this.setState({
	        sort: '',
	        direction: '',
	      });
	      break;
	  }
  }


  stateChange = (bool) => {
    if (!bool) {
      this.setState({
        loader: false,
      });
    }
  }

  stateTrue= (cat) => {
    if (this.state.selectedCat === null) {
      this.setState({
        loader: true,
        selectedCat: cat,
      });
    } else if (this.state.selectedCat !== cat) {
      this.setState({
        loader: true,
        selectedCat: cat,
      });
    } else {
      console.log('none');
    }
  }

  viewProducts = () => {
    let { products } = this.props;
    switch (this.state.sort) {
      case 'name':
        switch (this.state.direction) {
          case 'as':
            products = _.orderBy(products, ['title'], ['asc']);
            break;
          case 'ds':
            products = _.orderBy(products, ['title'], ['desc']);
            break;
          default:

            break;
        }
        break;
      case 'price':
        switch (this.state.direction) {
          case 'as':
            products = _.orderBy(products, ['price'], ['asc']);
            break;
          case 'ds':
            products = _.orderBy(products, ['price'], ['desc']);
            break;
          default:

            break;
        }
        break;
      default:

        break;
    }

    if (!this.state.loader) {
      return (products.map(prod => (

        <Col className="cf animateLayout" xs={20} sm={11} md={7} lg={5} xl={5} style={{ margin: '10px 10px', textAlign: 'center' }} key={prod.id}>
          <Link to={{ pathname: '/product', search: `?id=${prod.id}` }}>
            <img
              className="bottom"
              src={prod.img_2}

              style={{ maxHeight: '500px', width: '100%' }}
              alt="cat"
            />
            <img className="top" src={prod.img_3} style={{ maxHeight: '500px', width: '100%' }} key={prod.id} alt="Product" />
          </Link>
          <div style={{ display: 'flex', justifyContent: 'center', margin: '10px' }}>
            <div
              className="iconStyle"
              style={{
              }}
            >
              <Icon type="shopping-cart" />
            </div>
            <div
              className="iconStyle"
              onClick={() => this.showQuickView(prod.id)}

            >
              <Icon type="eye" theme="filled" />
            </div>

          </div>
          <div style={{ margin: '10px 10px', textAlign: 'center' }}>
            <strong>{prod.title}</strong>
            <p style={{ margin: 0 }}>{prod.price}</p>
            <StarRatings
              rating={5}
              starRatedColor="red"
          // changeRating={this.changeRating}
              numberOfStars={5}
              name="rating"
              starDimension="12px"
              starSpacing="2px"
            />
            <ColorBox colors={prod.variation_set} />
          </div>

        </Col>

      )));
    }
    console.log(products);
    return (
      <Spinner height="60vh" />
    );
  }

  viewCategory = () => (this.props.category.map(cat => (
    <div
      className="hover"
      onClick={() => this.stateTrue(`${cat.title}`)
            }
      role="presentation"
      key={cat.title}
    >
      <Link to={{ pathname: '/collections', search: `?gender=${this.state.gender || 'women'}&category=${cat.title}` }}>
        {cat.title}
      </Link>
    </div>
  )))

  quickViewProduct = () => {
    const product = _.find(this.props.products, ['id', this.state.quickViewId]);
    if (product && product.length !== 0) {
      return (
        <QuickView product={product} />
      );
    }
  }


  render() {
	  if (this.props.products.length === 0) {
	    return (
  <Spinner />
	    );
	  }
	  return (
  <div>
    <div style={{

    }}
    >

      <Modal
        contentLabel="Basic Modal"
        isOpen={this.state.quickView}
        style={customStyles}
        onRequestClose={this.closeModal}
      >
        <div>
          <Row type="flex" justify="end">
            <Icon type="close" onClick={this.closeModal} />
          </Row>
          {this.quickViewProduct()}
        </div>

      </Modal>
    </div>
    <Header category={this.props.category} gender={this.state.gender} />

    <Drawer title="Filter" side="left" onClose={this.onXClose} visible={this.state.visible}>

      <div style={{ marginBottom: '45px' }}>
        <h2>
							By Category
        </h2>
        <div>
          {this.viewCategory()}
        </div>
      </div>
      <div style={{ marginBottom: '45px' }}>
        <h2>
							By Color
        </h2>
        <div>
          <Link to={{ pathname: '/collections', search: `?gender=${this.state.gender || 'women'}&variation__color=Black&category=${this.state.categoryName === 'All Products' ? '' : this.state.categoryName}` }}>
            <div
              className="colorBox"
              style={{ backgroundColor: 'black' }}
            />
          </Link>

          <Link to={{ pathname: '/collections', search: `?gender=${this.state.gender || 'women'}&variation__color=Blue&category=${this.state.categoryName === 'All Products' ? '' : this.state.categoryName}` }}>
            <div
              className="colorBox"
              style={{ backgroundColor: 'blue' }}
            />
          </Link>

          <Link to={{ pathname: '/collections', search: `?gender=${this.state.gender || 'women'}&variation__color=green&category=${this.state.categoryName}` }}>
            <div
              className="colorBox"
              style={{ backgroundColor: 'green' }}
            />
          </Link>

          <Link to={{ pathname: '/collections', search: `?gender=${this.state.gender || 'women'}&variation__color=Grey&category=${this.state.categoryName}` }}>
            <div
              className="colorBox"
              style={{ backgroundColor: 'grey' }}
            />
          </Link>
          <div
            className="colorBox"
            style={{ backgroundColor: '#87D068' }}
          />
          <div
            className="colorBox"
            style={{ backgroundColor: '#bbb' }}
          />

          <div
            className="colorBox"
            style={{ backgroundColor: '#bbb' }}
          />

          <div
            className="colorBox"
            style={{ backgroundColor: '#bbb' }}
          />

        </div>
      </div>
      <div style={{ marginBottom: '45px' }}>
        <h2>
							By Size
        </h2>
        <div>
          <span className="hoverBox">XL</span>
          <span className="hoverBox">L</span>
          <span className="hoverBox">M</span>
          <span className="hoverBox">S</span>
        </div>
      </div>
      <div style={{ marginBottom: '45px' }}>
        <h2>
							By Price
        </h2>
        <div>
          <Slider range defaultValue={[20, 50]} />
        </div>
      </div>
    </Drawer>
    <Row className="categoryHead" type="flex" justify="center">
      <Col xs={24} sm={24} md={19} lg={19} xl={19}>
        <h1>
          {this.state.categoryName}
        </h1>
        <div>
        The very latest in globally-recognised clothing brands offers you with an exquisite range of Branded Clothes, Artificial Jewelry, Shoes, Beauty Products and Accessories for Women, Men & Kids at affordable price
        </div>
      </Col>

    </Row>
    <Row type="flex" justify="center">
      <Col xs={24} sm={24} md={19} lg={19} xl={19} style={{ padding: '0 30px', height: '40px' }}>
        <Button onClick={this.showDrawer} style={{ position: 'absolute', left: '22.5px' }}><Icon type="filter" />Filter</Button>
        <Select
          style={{ width: 200, position: 'absolute', right: '22.5px' }}
          showSearch
          placeholder="Sort By"
          optionFilterProp="children"
          filterOption={
            (input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
    }
          // value={}
          onChange={this.handleSorting}
        >
          <Option value="nameAs">Name Ascending</Option>
          <Option value="nameDs">Name Descending</Option>
          <Option value="priceAs">Price Ascending</Option>
          <Option value="priceDs">Price Descending</Option>

        </Select>
      </Col>

    </Row>
    <Row type="flex" justify="center">
      <Col
        xs={21}
        sm={21}
        lg={18}
        xl={18}
        style={{
          justifyContent: 'space-evenly', display: 'flex', flexWrap: 'wrap', flexDirection: 'row',
        }}
      >
        {this.viewProducts()}
      </Col>
    </Row>
  </div>
	  );
  }
}

const mapDispatchToProps = dispatch => ({

  dispatchFilter: filter => dispatch(productFilter(filter)),
  dispatchCategory: () => dispatch(productCategory()),
  dispatchSingleFilter: () => dispatch(productSingleFilter({ miscId: 1 })),
});

const mapStateToProps = state => ({
  products: state.products, category: state.category, product: state.products,
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductLayout);
